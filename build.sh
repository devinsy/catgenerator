#/bin/bash

#
# Display help.
#
function help
{
	echo "catgenerator build script."
	echo "Usage: build.sh [ -h | -help | --help | -snapshot | -local | -full ]"
	echo "     -h, -help, --help            display this help."
	echo "     -snapshot, --snapshot        build a snapshot."
	echo "     -local, --local              build a new version without tag nor version number commit nor Git push."
	echo "     -tagandpush, --tagandpush    build a new version with tag, version number commit and Git push."
	echo ""
}

#
# Build snapshot.
#
function build_snapshot
{
	okCount=0
	
	# Ant check.
	antCheck=`which ant`
	if [[ "$antCheck" =~ ^/.* ]]; then
		echo "Ant requirement................ OK"
		let "okCount+=1"
	else
		echo "Ant requirement................ MISSING"
	fi
	
	# Javac check.
	javacCheck=`which javac`
	if [[ "$javacCheck" =~ ^/.* ]]; then
		echo "Javac requirement.............. OK"
		let "okCount+=1"
	else
		echo "Javac requirement.............. MISSING"
	fi
	
	# Java version check.
	javaVersionCheck=`javac -version 2>&1`
	if [[ "$javaVersionCheck" =~ ^.*\ 11. ]]; then
		echo "Java 11 version requirement..... OK"
		let "okCount+=1"
	else
		echo "Java 11 version requirement..... MISSING"
	fi
	
	if [ "$okCount" == 3 ]; then
	    echo "Requirement OK"
	    ant -f build-snapshot.xml
	else
	    echo "Requirement MISSING, build abort"
	fi
}

#
# Build local.
#
function build_local
{
	okCount=0
	
	# Ant check.
	antCheck=`which ant`
	if [[ "$antCheck" =~ ^/.* ]]; then
		echo "Ant requirement................ OK"
		let "okCount+=1"
	else
		echo "Ant requirement................ MISSING"
	fi
	
	# Javac check.
	javacCheck=`which javac`
	if [[ "$javacCheck" =~ ^/.* ]]; then
		echo "Javac requirement.............. OK"
		let "okCount+=1"
	else
		echo "Javac requirement.............. MISSING"
	fi
	
	# Java version check.
	javaVersionCheck=`javac -version 2>&1`
	if [[ "$javaVersionCheck" =~ ^.*\ 11. ]]; then
		echo "Java 11 version requirement..... OK"
		let "okCount+=1"
	else
		echo "Java 11 version requirement..... MISSING"
	fi
	
	if [ "$okCount" == 3 ]; then
	    echo "Requirement OK"
	    ant -f build-local.xml
	else
	    echo "Requirement MISSING, build abort"
	fi
}

#
# Build tagandpush.
#
function build_tagandpush
{
	okCount=0
	
	# Ant check.
	antCheck=`which ant`
	if [[ "$antCheck" =~ ^/.* ]]; then
		echo "Ant requirement................ OK"
		let "okCount+=1"
	else
		echo "Ant requirement................ MISSING"
	fi
	
	# Javac check.
	javacCheck=`which javac`
	if [[ "$javacCheck" =~ ^/.* ]]; then
		echo "Javac requirement.............. OK"
		let "okCount+=1"
	else
		echo "Javac requirement.............. MISSING"
	fi
	
	# Java version check.
	javaVersionCheck=`javac -version 2>&1`
	if [[ "$javaVersionCheck" =~ ^.*\ 11. ]]; then
		echo "Java 11 version requirement..... OK"
		let "okCount+=1"
	else
		echo "Java 11 version requirement..... MISSING"
	fi
	
	# Git check.
	gitCheck=`which git 2>&1`
	if [[ "$gitCheck" =~ ^/.* ]]; then
		echo "GIT requirement................ OK"
		let "okCount+=1"
	else
		echo "GIT requirement................ MISSING"
	fi

	if [ "$okCount" == 4 ]; then
	    echo "Requirement OK"
	    ant -f build-tagandpush.xml
	else
	    echo "Requirement MISSING, build abort"
	fi
}

#
# Main.
#
if [ "$#" -eq 0 ] || [ "$1" == "-h" ]  || [ "$1" == "-help" ]  || [ "$1" == "--help" ]; then
    help
elif [ "$1" == "-snapshot" ] || [ "$1" == "--snapshot" ] ; then
    build_snapshot
elif [ "$1" == "-local" ] || [ "$1" == "--local" ] ; then
    build_local
elif [ "$1" == "-tagandpush" ] || [ "$1" == "--tagandpush" ] ; then
    build_tagandpush
else
    echo "Invalid parameters."
    help
fi


