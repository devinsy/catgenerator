# CatGenerator

CatGenerator is a Java version of the Cat avatar Generator by David Revoy.

Many thanks to David Revoy and Andreas Gohr <3

## README of Cat avatar Generator

Source: https://www.peppercarrot.com/extras/html/2016_cat-generator/

```
Cat avatar Generator
====================

Source code and graphics CC-By [1] David Revoy [2] ( artworks of Cats and open-raster file )
Originally inspired of the code for "MonsterID" by Andreas Gohr [3].

If you use this software and/or graphics please link back to http://www.splitbrain.org/go/monsterid, http://www.peppercarrot.com

How to edit:
============
1. Open img/00_SRC.ora with Krita. Do your edit/draw/paint, respect layer naming, save.
2. Open it again in Gimp, with the 'export layer plugin' [4]
3. Scale the image down to the result you want (eg. 256px x 256px as on the demo ) 
3. File > Export layer. Allow invisible layer to be exported, check 'image size', PNG
4. Done. PNG files of 'parts' extracted.

[1] http://creativecommons.org/licenses/by/4.0/
[2] http://www.peppercarrot.com
[3] https://www.splitbrain.org/projects/monsterid
[4] https://github.com/khalim19/gimp-plugin-export-layers/releases/download/2.4/export-layers-2.4.zip
```


## LICENSE

CatGenerator is released under the GNU AGPL+ license. Enjoy!

Artworks of Cats and open-raster file are released under the Creative Commons CC-By-4.0 David Revoy (http://creativecommons.org/licenses/by/4.0/).

## AUTHOR

Christian Pierre MOMON <christian.momon@devinsy.fr>

## DOCUMENTATION



## Requirements

- Java 11
- Eclipse 4.16 (202006).

## INSTALL

TODO

### Unit test environment
For unit tests, install the TestNG: 
* https://marketplace.eclipse.org/content/testng-eclipse
* Eclipse menu > Help > Eclipse Marketplace > Find "TestNG" > TestNG for Eclipse: Install button

## LOGO

Generated with CatGenerator by Christian Pierre MOMON <christian.momon@devinsy.fr>.

License: Creative Commons CC-BY-SA-4.0.
