/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 *
 * This file is part of CatGenerator, simple service statistics tool.
 *
 * CatGenerator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CatGenerator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with CatGenerator.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.catgenerator;

import java.io.File;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.EnhancedPatternLayout;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.catgenerator.cli.CatGeneratorCLI;

/**
 * The Class CatGeneratorLauncher.
 */
public final class CatGeneratorLauncher
{
    private static Logger logger = LoggerFactory.getLogger(CatGeneratorLauncher.class);

    /**
     * Instantiates a new cat generator launcher.
     */
    private CatGeneratorLauncher()
    {
    }

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     */
    public static void main(final String[] args)
    {
        // Configure log.
        File loggerConfig = new File("log4j.properties");
        if (loggerConfig.exists())
        {
            PropertyConfigurator.configure(loggerConfig.getAbsolutePath());
            logger.info("Dedicated log configuration done.");
            logger.info("Configuration file was found in [{}].", loggerConfig.getAbsoluteFile());
        }
        else
        {
            BasicConfigurator.configure(new ConsoleAppender(new EnhancedPatternLayout("%m%n")));
        }

        // Run.
        CatGeneratorCLI.run(args);
    }
}
