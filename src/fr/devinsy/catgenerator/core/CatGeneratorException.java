/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 * 
 * This file is part of CatGenerator, simple service statistics tool.
 * 
 * CatGenerator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * CatGenerator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with CatGenerator.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.catgenerator.core;

/**
 * The Class JugaException.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class CatGeneratorException extends Exception
{
	private static final long serialVersionUID = -2760108468711463449L;

	/**
     * Instantiates a new Juga exception.
     */
    public CatGeneratorException()
    {
        super();
    }

    /**
     * Instantiates a new Juga exception.
     * 
     * @param message
     *            the message
     */
    public CatGeneratorException(final String message)
    {
        super(message);
    }

    /**
     * Instantiates a new Juga exception.
     * 
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public CatGeneratorException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Instantiates a new Juga exception.
     * 
     * @param cause
     *            the cause
     */
    public CatGeneratorException(final Throwable cause)
    {
        super(cause);
    }
}
