/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 *
 * This file is part of CatGenerator, simple service statistics tool.
 *
 * CatGenerator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CatGenerator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with CatGenerator.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.catgenerator.core;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CatGeneratorUtils.
 */
public class CatGeneratorUtils
{
    private static Logger logger = LoggerFactory.getLogger(CatGeneratorUtils.class);

    /**
     * Resize.
     *
     * @param image
     *            the image
     * @param width
     *            the width
     * @param height
     *            the height
     * @return the buffered image
     */
    public static BufferedImage resize(final BufferedImage image, final int width, final int height)
    {
        BufferedImage result;

        result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = result.createGraphics();
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();

        //
        return result;
    }
}
