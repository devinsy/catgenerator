/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 *
 * This file is part of CatGenerator, simple service statistics tool.
 *
 * CatGenerator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CatGenerator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with CatGenerator.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.catgenerator.core;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CatGenerator.
 */
public class BirdGenerator
{
    private static Logger logger = LoggerFactory.getLogger(BirdGenerator.class);

    public static final int DEFAULT_SIZE = 256;

    /**
     * Builds the avatar.
     *
     * @return the buffered image
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static BufferedImage buildAvatar() throws IOException
    {
        BufferedImage result;

        result = buildAvatar(null, null);

        //
        return result;
    }

    /**
     * Builds the avatar.
     *
     * @param size
     *            the size
     * @return the buffered image
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static BufferedImage buildAvatar(final int size) throws IOException
    {
        BufferedImage result;

        result = buildAvatar(null, size);

        //
        return result;
    }

    /**
     * Builds the avatar.
     *
     * @param seed
     *            the seed
     * @return the buffered image
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static BufferedImage buildAvatar(final String seed) throws IOException
    {
        BufferedImage result;

        result = buildAvatar(seed, null);

        //
        return result;
    }

    /**
     * Builds the cat.
     *
     * @param seed
     *            the seed
     * @param size
     *            the size
     * @return the buffered image
     * @throws IOException
     */
    public static BufferedImage buildAvatar(final String seed, final Integer size) throws IOException
    {
        BufferedImage result;

        //
        String targetSeed;
        if (StringUtils.isBlank(seed))
        {
            targetSeed = RandomStringUtils.randomAlphanumeric(10);
        }
        else
        {
            targetSeed = seed;
        }

        //
        int targetSize;
        if ((size == null) || (size < 1))
        {
            targetSize = DEFAULT_SIZE;
        }
        else
        {
            targetSize = size;
        }

        // Create an empty image.
        result = new BufferedImage(DEFAULT_SIZE, DEFAULT_SIZE, BufferedImage.TYPE_INT_ARGB);
        Graphics2D grapher = result.createGraphics();
        grapher.setBackground(Color.WHITE);
        grapher.clearRect(0, 0, DEFAULT_SIZE, DEFAULT_SIZE);

        // Add stuff.
        // Initialize the random generator.
        Random generator = new Random(Integer.parseInt(DigestUtils.md5Hex(targetSeed).substring(0, 6), 16));
        drawImage(grapher, "tail", generator.nextInt(9) + 1);
        drawImage(grapher, "hoop", generator.nextInt(10) + 1);
        drawImage(grapher, "body", generator.nextInt(9) + 1);
        drawImage(grapher, "wing", generator.nextInt(9) + 1);
        drawImage(grapher, "eyes", generator.nextInt(9) + 1);
        drawImage(grapher, "bec", generator.nextInt(9) + 1);
        drawImage(grapher, "accessorie", generator.nextInt(20) + 1);

        grapher.dispose();

        //
        if (targetSize != DEFAULT_SIZE)
        {
            result = CatGeneratorUtils.resize(result, targetSize, targetSize);
        }

        //
        return result;
    }

    /**
     * Generate cat logo.
     *
     * @param seed
     *            the seed
     * @param target
     *            the target
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void buildAvatarFromPeeperCarrotWebsite(final String seed, final File target) throws IOException
    {
        String targetSeed;
        if (StringUtils.isBlank(seed))
        {
            targetSeed = "";
        }
        else
        {
            targetSeed = seed;
        }

        URL source = new URL("https://www.peppercarrot.com/extras/html/2016_cat-generator/avatar.php?seed=" + targetSeed);
        FileUtils.copyURLToFile(source, target, 5000, 5000);
    }

    /**
     * Builds the avatar to.
     *
     * @param target
     *            the target
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void buildAvatarTo(final File target) throws IOException
    {
        buildAvatarTo(null, null, target);
    }

    /**
     * Builds the avatar to.
     *
     * @param size
     *            the size
     * @param target
     *            the target
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void buildAvatarTo(final int size, final File target) throws IOException
    {
        buildAvatarTo(null, size, target);
    }

    /**
     * Builds the avatar to.
     *
     * @param seed
     *            the seed
     * @param target
     *            the target
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void buildAvatarTo(final String seed, final File target) throws IOException
    {
        buildAvatarTo(seed, null, target);
    }

    /**
     * Builds the avatar to.
     *
     * @param seed
     *            the seed
     * @param size
     *            the size
     * @param target
     *            the target
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void buildAvatarTo(final String seed, final Integer size, final File target) throws IOException
    {
        BufferedImage result = buildAvatar(seed, size);
        ImageIO.write(result, FilenameUtils.getExtension(target.getName()), target);
    }

    /**
     * Draw image.
     *
     * @param grapher
     *            the grapher
     * @param name
     *            the name
     * @param index
     *            the index
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private static void drawImage(final Graphics2D grapher, final String name, final int index) throws IOException
    {
        String fileName = "/fr/devinsy/catgenerator/core/images/bird/" + name + "_" + index + ".png";
        URL url = BirdGenerator.class.getResource(fileName);
        BufferedImage part = ImageIO.read(url);
        grapher.drawImage(part, null, 0, 0);
    }
}
