/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 *
 * This file is part of CatGenerator, simple service statistics tool.
 *
 * CatGenerator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CatGenerator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with CatGenerator.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.catgenerator.cli;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.catgenerator.core.BirdGenerator;
import fr.devinsy.catgenerator.core.CatGenerator;
import fr.devinsy.catgenerator.util.BuildInformation;
import fr.devinsy.strings.StringList;

/**
 * The Class CatGeneratorCLI.
 */
public final class CatGeneratorCLI
{
    private static Logger logger = LoggerFactory.getLogger(CatGeneratorCLI.class);

    /**
     * Instantiates a new cat generator CLI.
     */
    private CatGeneratorCLI()
    {
    }

    /**
     * Display help.
     */
    public static void displayHelp()
    {
        StringList message = new StringList();

        message.append("CatGenerator CLI ").appendln(BuildInformation.instance().version());
        message.appendln("Usage:");
        message.appendln("    catgenerator [OPTIONS] <filename>   Build cat avatar to file.");
        message.appendln("");
        message.appendln("Options:");
        message.appendln("    -h, -help, --help        Displays this help.");
        message.appendln("    -v, -version, --version  Displays version information.");
        message.appendln("    -size <value>            Resizes the avatar image. Default is 256.");
        message.appendln("    -k <value>               Determinizes the build from a keyword. Default is random.");
        message.appendln("    -cat                     To generate a cat avatar. Default.");
        message.appendln("    -bird                    To generate a bird avatar.");
        message.appendln("");
        message.appendln("Arguments:");
        message.appendln("    filename                 The target file name. Default extension is .png.");
        message.appendln("");
        message.appendln("Originally inspired of:");
        message.appendln("    - Cat avatar Generator by David Revoy: http://www.peppercarrot.com/ ");
        message.appendln("    - MonsterID by Andreas Gohr: http://www.splitbrain.org/go/monsterid");
        message.appendln("Licenses:");
        message.appendln("    - CC-BY 4.0 for artworks of Cats and open-raster file");
        message.appendln("    - GNU AGPL+ for Java code (https://forge.devinsy.fr/devinsy/catgenerator).");

        logger.info(message.toString());
    }

    /**
     * Display version.
     */
    public static void displayVersion()
    {
        StringList message = new StringList();

        message.appendln(BuildInformation.instance().version());

        logger.info(message.toString());
    }

    /**
     * Checks if is matching.
     *
     * @param args
     *            the args
     * @param regexps
     *            the regexps
     * @return true, if is matching
     */
    public static boolean isMatching(final String[] args, final String... regexps)
    {
        boolean result;

        if ((args.length == 0) && (regexps == null))
        {
            result = true;
        }
        else if ((args.length != 0) && (regexps == null))
        {
            result = false;
        }
        else if (args.length != regexps.length)
        {
            result = false;
        }
        else
        {
            boolean ended = false;
            int index = 0;
            result = false;
            while (!ended)
            {
                if (index < args.length)
                {
                    String arg = args[index];
                    String regexp = regexps[index];

                    if (arg.matches(regexp))
                    {
                        index += 1;
                    }
                    else
                    {
                        ended = true;
                        result = false;
                    }
                }
                else
                {
                    ended = true;
                    result = true;
                }
            }
        }

        //
        return result;
    }

    /**
     * Normalize file.
     *
     * @param source
     *            the source
     * @return the file
     */
    private static File normalizeFile(final String source)
    {
        File result;

        if (StringUtils.isBlank(FilenameUtils.getExtension(source)))
        {
            result = new File(source + ".png");
        }
        else
        {
            result = new File(source);
        }

        //
        return result;
    }

    /**
     *
     * This method launch CLI.
     *
     * @param args
     *            necessary arguments
     */
    public static void run(final String[] args)
    {
        // Set default catch.
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(final Thread thread, final Throwable exception)
            {
                String message;
                if (exception instanceof OutOfMemoryError)
                {
                    message = "Java ran out of memory!\n\n";
                }
                else
                {
                    message = String.format("An error occured: %1s(%2s)", exception.getClass(), exception.getMessage());
                }

                logger.error("uncaughtException ", exception);
                logger.error(message);
                logger.info("Oups, an unexpected error occured. Please try again.");
            }
        });

        try
        {
            logger.info("{} catgenerator call: {}", LocalDateTime.now(), new StringList(args).toStringSeparatedBy(" "));

            if (isMatching(args))
            {
                logger.info("No parameter.");
                displayHelp();
            }
            else if (isMatching(args, "(-h|--h|--help)"))
            {
                displayHelp();
            }
            else if (isMatching(args, "(-v|-version|--version)"))
            {
                displayVersion();
            }
            else if (isMatching(args, ".+"))
            {
                logger.info("Matching 4.");
                CatGenerator.buildAvatarTo(normalizeFile(args[0]));
            }
            else
            {
                String regex = "(((?<bird>-bird)?|(-cat)?|(-size (?<size>\\d+))?|(-k (?<keyword>\\S+))?) )+(?<filename>.+)";
                Pattern pattern = Pattern.compile(regex);

                Matcher matcher = pattern.matcher(toCommandLine(args));
                if (matcher.matches())
                {
                    String fileName = matcher.group("filename");
                    Integer size = NumberUtils.toInt(matcher.group("size"), -1);
                    String bird = matcher.group("bird");
                    String keyword = matcher.group("keyword");

                    if (bird == null)
                    {
                        CatGenerator.buildAvatarTo(keyword, size, normalizeFile(fileName));
                    }
                    else
                    {
                        BirdGenerator.buildAvatarTo(keyword, size, normalizeFile(fileName));
                    }
                }
                else
                {
                    logger.info("Bad usage.");
                    displayHelp();
                }
            }
        }
        catch (IOException exception)
        {
            logger.error("Error detected: {}", exception.getMessage());
            exception.printStackTrace();
        }

        //
        logger.info("Done.");
    }

    /**
     * To command line.
     *
     * @param args
     *            the args
     * @return the string
     */
    public static String toCommandLine(final String[] args)
    {
        String result;

        StringList buffer = new StringList();
        for (String string : args)
        {
            if (string.contains(" "))
            {
                buffer.append("\"" + string + "\"");
            }
            else
            {
                buffer.append(string);
            }
        }

        result = buffer.toStringSeparatedBy(' ');

        //
        return result;
    }
}
