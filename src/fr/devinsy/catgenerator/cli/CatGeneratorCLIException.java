/*
 * Copyright (C) 2020 Christian Pierre MOMON <christian@momon.org>
 *
 * This file is part of CatGenerator, simple service statistics tool.
 *
 * CatGenerator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CatGenerator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with CatGenerator.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.catgenerator.cli;

import fr.devinsy.catgenerator.core.CatGeneratorException;

/**
 * The Class CatGeneratorCLIException.
 */
public class CatGeneratorCLIException extends CatGeneratorException
{
    private static final long serialVersionUID = 7939169842976753439L;

    /**
     * Instantiates a new cat generator CLI exception.
     */
    public CatGeneratorCLIException()
    {
        super();
    }

    /**
     * Instantiates a new cat generator CLI exception.
     *
     * @param message
     *            the message
     */
    public CatGeneratorCLIException(final String message)
    {
        super(message);
    }

    /**
     * Instantiates a new cat generator CLI exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public CatGeneratorCLIException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Instantiates a new cat generator CLI exception.
     *
     * @param cause
     *            the cause
     */
    public CatGeneratorCLIException(final Throwable cause)
    {
        super(cause);
    }
}